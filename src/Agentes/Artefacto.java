/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agentes;

import GUI.VentanaAgentesJF;
import static GUI.VentanaAgentesJF.tempJTBL;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.awt.Color;

/**
 *
 * @author kfsarango1
 */
public class Artefacto extends Agent {

    @Override
    protected void setup() {
        
        //Llamando a la ventana de muestra el comportamiento de los agentes
        VentanaAgentesJF interfaz = new VentanaAgentesJF();
        interfaz.show();
        
        //Cada ves que recibe un mensaje de temperatura
        addBehaviour(new CyclicBehaviour() { 
            @Override
            public void action() {
                //Estableciendo el intervalo de una temperatura adecuado
                int tempMin = 17;
                int tempMax = 27;

                //Estableciendo intervalo de temperatura en la GUI
                VentanaAgentesJF.tempJTBL.setValueAt(String.valueOf(tempMin), 0, 0);
                VentanaAgentesJF.tempJTBL.setValueAt(String.valueOf(tempMax), 0, 1);

                //Recibiendo el valor de temperatura
                ACLMessage message = receive();

                //Commprobando si el mensaje recibido es válido
                if (message != null) {
                    //Convirtiendo el valor de temperatura  a un entero
                    double temperaturaRecibida = Double.parseDouble(message.getContent());

                    //Modificando componentes de la interfaz VentanaAgentes
                    VentanaAgentesJF.temperaturaTXF.setText(String.format("%.1f", temperaturaRecibida) + "°C");

                    if (temperaturaRecibida >= tempMin && temperaturaRecibida <= tempMax) {
                        //Modificando componentes de la interfaz VentanaAgentes
                        VentanaAgentesJF.estadoTXF.setText("Temperatura adecuada");
                        VentanaAgentesJF.estadoTXF.setFont(new java.awt.Font("Ubuntu Mono", 1, 36));
                        VentanaAgentesJF.tempJTBL.setBackground(new java.awt.Color(31, 231, 107));
                    } else if (temperaturaRecibida < tempMin) {
                        //Modificando componentes de la interfaz VentanaAgentes
                        VentanaAgentesJF.estadoTXF.setText("Es necesario encender la calefacción");
                        VentanaAgentesJF.estadoTXF.setFont(new java.awt.Font("Ubuntu Mono", 0, 24));
                        VentanaAgentesJF.tit_calefaccionLBL.setForeground(Color.black);
                        VentanaAgentesJF.heatingLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/heating.png")));
                    } else if (temperaturaRecibida > tempMax) {
                        //Modificando componentes de la interfaz VentanaAgentes
                        VentanaAgentesJF.estadoTXF.setText("Es necesario encender el aire acondicionado");
                        VentanaAgentesJF.estadoTXF.setFont(new java.awt.Font("Ubuntu Mono", 0, 24));
                        VentanaAgentesJF.air_conditionaterLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Img/minisplit.png")));
                        VentanaAgentesJF.tit_aireLBL.setForeground(Color.black);
                    }
                } else {
                    block();
                }
            }
        });
    }

}
